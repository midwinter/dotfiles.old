#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    *)          machine="UNKNOWN:${unameOut}"
esac

echo "Installing dotfiles for $machine"

if [[ "$machine" == "Linux" ]]; then
	packageManager="sudo apt"
elif [[ "$machine" == "Mac" ]]; then
	packageManager=brew
	echo "Checking for homebrew installation"
	which -s brew
	if [[ $? != 0 ]] ; then
		echo "Homebrew not found. Installing..."
		ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	else
		echo "Homebrew already installed! Updating..."
		brew update
	fi
fi
echo "installing zsh"
$packageManager install zsh 


if [ -e $HOME/.zshrc  ] || [ -e $HOME/.oh-my-zsh ]; then
  today=$(date +%F)
  echo "=== Zsh files detected, backing old ones up if not symlinked"

  if [ -L $HOME/.zshrc ]; then
    echo "... Ignoring symlinked zshrc config"
  else
    if [ -e $HOME/.zshrc ]; then
      echo "... Backing up $HOME/.zshrc to $HOME/zshrc_$today"
      mv $HOME/.zshrc $HOME/zshrc_$today
    fi
  fi

    if [ -d $HOME/.oh-my-zsh ]; then
      echo "... Backing up $HOME/.oh-my-zsh to $HOME/oh-my-zsh_$today"
      rm -rf $HOME/oh-my-zsh_$today
      mkdir $HOME/oh-my-zsh_$today
      cp -R $HOME/.oh-my-zsh $HOME/oh-my-zsh_$today/
      rm -rf $HOME/.oh-my-zsh
    fi
else
  echo "=== Installing zsh config files"
fi
if sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"; then
	echo "oh-my-zsh installed"
	rm $HOME/.zshrc 
else
	echo "oh-my-zsh not installed"
	exit 1
fi

echo "=== Linking zsh files"
if [ -L $HOME/.zshrc ]; then
  echo "... Ignoring symlinked zshrc config"
else
  ln -s $PWD/zshrc $HOME/.zshrc
fi
pip install --user powerline-status
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k


echo "installing vim"
[ "$packageManager install vim" ];

if [ -d $PWD/vim ]; then
  echo "=== Local vim folder exists"
else
  echo "=== Creating local vim folder"
  mkdir $PWD/vim
fi

if [ -e $HOME/.vimrc ] || [ -e $HOME/.vim ]; then
  today=$(date +%F)
  echo "=== Vim files detected, backing old ones up if not symlinked"

  if [ -L $HOME/.vimrc ]; then
    echo "... Ignoring symlinked Vim config"
  else
    if [ -e $HOME/.vimrc ]; then
      echo "... Backing up $HOME/.vimrc to $HOME/vimrc_$today"
      mv $HOME/.vimrc $HOME/vimrc_$today
    fi
  fi

  if [ -L $HOME/.vim ]; then
    echo "... Ignoring symlinked Vim directory"
  else
    if [ -d $HOME/.vim ]; then
      echo "... Backing up $HOME/.vim to $HOME/vim_$today"
      rm -rf $HOME/vim_$today
      mkdir $HOME/vim_$today
      cp -R $HOME/.vim $HOME/vim_$today/
      rm -rf $HOME/.vim
    fi
  fi
else
  echo "=== Installing Vim config files"
fi

echo "=== Linking Vim files"
if [ -L $HOME/.vimrc ]; then
  echo "... Ignoring symlinked Vim config"
else
  ln -s $PWD/vimrc $HOME/.vimrc
fi

if [ -L $HOME/.vim ]; then
  echo "... Ignoring symlinked Vim directory"
else
  ln -s $PWD/vim $HOME/.vim
fi

if [ -e $HOME/.vim/autoload/plug.vim ]; then
  echo "... Plug already installed"
else
  echo "=== Installing Plug"
  curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

echo "=== Installing plugins"
vim +PlugInstall +qall

echo "=== Done"
