"------------------------------------------------------------------------------
" GENERAL
"------------------------------------------------------------------------------
set encoding=utf-8                  " Ensure encoding is UTF-8
set nocompatible                    " Disable Vi compatability
set shell=/bin/bash                 " Ensure bash is used for execution
set wildmode=list:longest,list:full " Ignore files in search
set wildignore+=*/tmp/*,env/*,.tmp,.nuxt,public_html,vendor,bower_components,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite,*.o,*.obj,.git,*.rbc,*/__pycache__/*,*/site-packages/*,node_modules,dist,build
set binary
set noeol                           " No automatic end of line additionS
set timeoutlen=1000 ttimeoutlen=0   " reduce timeout required for key to register
set hidden
set smartcase
set hlsearch

if &term =~ '256color'
  set t_ut=
endif

if has("termguicolors")     " set true colors
    set t_8f=\[[38;2;%lu;%lu;%lum
    set t_8b=\[[48;2;%lu;%lu;%lum
    set termguicolors
endif

"------------------------------------------------------------------------------
" Plugins
"------------------------------------------------------------------------------
"
call plug#begin()
Plug 'drewtempelmeyer/palenight.vim'
Plug 'tpope/vim-sensible'
Plug 'jlanzarotta/bufexplorer'
" On-demand loading
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'kien/ctrlp.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'thaerkh/vim-workspace'
Plug 'junegunn/vim-emoji'
Plug 'airblade/vim-gitgutter'
Plug 'morhetz/gruvbox'			" colorscheme
Plug 'dyng/ctrlsf.vim'                " Sublime text style search window
Plug 'vim-scripts/grep.vim'           " Grep search of files
Plug 'owickstrom/vim-colors-paramount'
Plug 'kblin/vim-fountain'
Plug 'justinmk/vim-sneak'
Plug 'joonty/vdebug'

call plug#end()


"""""""""""""""""""
""" Init/conf Stuff
"""""""""""""""""""



"""
""" Colors
"""
hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE
"set background=dark
"set t_Co=256 " 256 colors in terminal

if has('gui_gnome')
  set guifont=Ubuntu\ Mono\ 8.4
endif

" Neovim
" let $NVIM_TUI_ENABLE_TRUE_COLOR=1 " not yet supported in iTerm 2 stable
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" Tweaks for Molokai colorscheme (ignored if Molokai isn't used)
let g:molokai_original=1
let g:rehash256=1

" Use the first available colorscheme in this list
"for scheme in [ 'palenight', 'paramount', 'gruvbox', 'solarized', 'molokai', 'desert' ]
"  try
"    execute 'colorscheme '.scheme
"    break
"  catch
"    continue
"  endtry
"endfor

" Highlight columns 80 and 120
"let &colorcolumn="80,".join(range(120,999),",")


"""
""" Line Numbers
"""
:set number relativenumber
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END


"""
""" Shell
"""
"set shell=zsh -i


"""
""" NERDTree
"""
let NERDTreeShowHidden=0 " Press I to toggle hidden files
let NERDTreeQuitOnOpen=1
let NERDTreeDirArrows=1
let NERDTreeChDirMode=2
let NERDTreeHighlightCursorline=1
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif


"""
""" CtrlP
"""
let g:ctrlp_max_files = 0
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']


"" The Silver Searcher
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  unlet g:ctrlp_user_command
  let g:ctrlp_custom_ignore = {
    \ 'dir':  '\.git\|\.hg\|\.svn\|\.nuxt\|\.tmp\|env\|bower_components\|dist\|node_modules\|__pycache__\|site-packages\|project_files\|public_html\|storage',
    \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$' }
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_use_caching = 0
endif

"" ripgrep
if executable('rg')
  set grepprg=rg\ --vimgrep
  unlet g:ctrlp_user_command
  let g:ctrlp_custom_ignore = {
    \ 'dir':  '\.git\|\.hg\|\.svn\|\.nuxt\|\.tmp\|env\|bower_components\|dist\|node_modules\|__pycache__\|site-packages\|project_files\|public_html\|storage',
    \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$|\.DS_STORE$' }
  let g:ctrlp_user_command = 'rg --files %s'
  let g:ctrlp_use_caching = 0
endif

"""
""" GitGutter
"""

"" Search in files
nmap     <leader>f <Plug>CtrlSFPrompt
vmap     <leader>f <Plug>CtrlSFVwordPath
vmap     <leader>F <Plug>CtrlSFVwordExec
nmap     <leader>n <Plug>CtrlSFCwordPath
nmap     <leader>p <Plug>CtrlSFPwordPath
nnoremap <leader>of :CtrlSFOpen<CR>
nnoremap <leader>tf :CtrlSFToggle<CR>
inoremap <leader>tf <Esc>:CtrlSFToggle<CR>

"""""""""""""""""""
""" Bindings
"""""""""""""""""""

" Toggles vim-workspace
nnoremap <leader>s :ToggleWorkspace<CR>
map <C-e> :NERDTreeToggle<CR> 			" Toggles NERDTree
nnoremap <silent> <leader>b :buffers<CR>
noremap <leader>c :bd<CR>

map f <Plug>Sneak_s
map F <Plug>Sneak_S

if (&tildeop)
  nmap gcw guw~l
  nmap gcW guW~l
  nmap gciw guiw~l
  nmap gciW guiW~l
  nmap gcis guis~l
  nmap gc$ gu$~l
  nmap gcgc guu~l
  nmap gcc guu~l
  vmap gc gu~l
else
  nmap gcw guw~h
  nmap gcW guW~h
  nmap gciw guiw~h
  nmap gciW guiW~h
  nmap gcis guis~h
  nmap gc$ gu$~h
  nmap gcgc guu~h
  nmap gcc guu~h
  vmap gc gu~h
endif